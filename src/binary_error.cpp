#include "myImageIO.h"
#include <iostream>

int optimize_thres(myImageData* &image)
{
  double total = 0;
  int W = image->getWidth();
  int H = image->getHeight();
  for(int y = 0; y < H; y++)
  {
    for (int x = 0; x < W; x++)
    {
      total += image->get(x,y);
    }
  }
  return total/(W*H);
}

int main(int argc, char **argv)
{
  // Initialize image class
  myImageData *img (new myImageData);
  img->read(argv[1]);

  double threshold = optimize_thres(img);

  std::cout << "threshold is :" << threshold << '\n';

  int W = img->getWidth();
  int H = img->getHeight();

  myImageData *binary (new myImageData);
  binary->init(W, H, 1);

  double k1 = 7.0/16;
  double k2 = 1.0/16;
  double k3 = 5.0/16;
  double k4 = 3.0/16;

  for(int y = 0; y < H; y++)
  {
    for (int x = 0; x < W; x++)
    {
      int error = 0;
      int input_val = img->get(x, y);
      if (input_val > threshold)
      {
        binary->set(x, y, 255);
        error = input_val - 255;
      } else {
        binary->set(x, y, 0);
        error = input_val - 0;
      }
      // Propagate the error
      if (x + 1 < W && y < H) img->set(x + 1, y, (img->get(x + 1, y) + (error*k1)));
      if (x + 1 < W && y + 1 < H) img->set(x + 1, y + 1, (img->get(x + 1, y + 1) + (error*k2)));
      if (x < W && y + 1 < H) img->set(x, y + 1, (img->get(x, y + 1) + (error*k3)));
      if (x - 1 < W && y + 1 < H) img->set(x - 1, y + 1, (img->get(x -1, y + 1) + (error*k4)));
    }
  }

  binary->save("binary_error");
}
