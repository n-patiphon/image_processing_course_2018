#include "myImageIO.h"

int main(int argc, char **argv)
{
  // Initialize image class
  myImageData *img (new myImageData);
  img->read(argv[1]);

  int W = img->getWidth();
  int H = img->getHeight();
  int C = img->getCH();

  myImageData *swap (new myImageData);
  swap->init(W, H, C);

  for(int y = 0; y < H; y++)
  {
    for (int x = 0; x < W; x++)
    {
      double R = img->get(x, y, 0);
      double G = img->get(x, y, 1);
      double B = img->get(x, y, 2);
      swap->set(x, y, 0, B);
      swap->set(x, y, 1, R);
      swap->set(x, y, 2, G);
    }
  }

  swap->save("swap");
}
