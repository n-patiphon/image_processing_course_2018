#include "myImageIO.h"
#include <iostream>

int optimize_thres(myImageData* &image)
{
  double total = 0;
  int W = image->getWidth();
  int H = image->getHeight();
  for(int y = 0; y < H; y++)
  {
    for (int x = 0; x < W; x++)
    {
      total += image->get(x,y);
    }
  }
  return total/(W*H);
}

int main(int argc, char **argv)
{
  // Initialize image class
  myImageData *img (new myImageData);
  img->read(argv[1]);

  double threshold = optimize_thres(img);

  std::cout << "threshold is :" << threshold << '\n';

  int W = img->getWidth();
  int H = img->getHeight();

  myImageData *binary (new myImageData);
  binary->init(W, H, 1);

  for(int y = 0; y < H; y++)
  {
    for (int x = 0; x < W; x++)
    {
      int input_val = img->get(x, y);
      if (input_val > threshold)
      {
        binary->set(x, y, 255);
      } else {
        binary->set(x, y, 0);
      }
    }
  }

  binary->save("binary");
}
