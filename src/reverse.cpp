#include "myImageIO.h"

int main(int argc, char **argv)
{
  // Initialize image class
  myImageData *img (new myImageData);
  img->read(argv[1]);

  int W = img->getWidth();
  int H = img->getHeight();

  myImageData *reverse (new myImageData);
  reverse->init(W, H, 1);

  for(int y = 0; y < H; y++)
  {
    for (int x = 0; x < W; x++)
    {
      double val = 255 - img->get(x, y);
      reverse->set(x, y, val);
    }
  }

  reverse->save("reverse");
}
