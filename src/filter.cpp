#include <stdio.h>
#include <stdlib.h>
#include "myImageIO.h"
#include <iostream>
#include <string>

double average[] 					{1.0/9, 1.0/9, 1.0/9,
													 1.0/9,	1.0/9, 1.0/9,
													 1.0/9,	1.0/9, 1.0/9};
double verical_edge[] 		{-1.0, 0.0, 1.0,
													 -2.0,	0.0, 2.0,
													 -1.0,	0.0, 1.0};
double horizontal_edge[] 	{-1.0, -2.0, -1.0,
													 0.0,	0.0, 0.0,
													 1.0, 2.0, 1.0};
double laplacian[] 				{1.0, 1.0, 1.0,
													 1.0,	-8.0, 1.0,
													 1.0,	1.0, 1.0};

void operation(myImageData * in, myImageData *out, double filter[]){

	int W = in->getWidth();
	int H = in->getHeight();
	int C = in->getCH();
	const int winsize = 1;

	for(int y = 0; y < H; y++){
		for(int x = 0; x < W; x++){

			int sum = 0;

			for(int j = -winsize; j <= winsize; j++){

				int yp = y + j;
				if(yp < 0) yp = yp + H;
				if(yp > H-1) yp = yp - H;

				for(int i = -winsize; i <= winsize; i++){

					int xp = x + i;
					if(xp < 0) xp = xp + W;
					if(xp > W-1) xp = xp - W;

					int index = (2*winsize + 1)*(j + 1) + (i + 1);

					double val_in = in->get(xp, yp) * filter[index];

					sum += val_in;

				}
			}

			out->set(x, y, sum);

		}

	}

}

int main(int argc, char **argv){

	// read image data to img1

	myImageData * img1 = new myImageData();
	img1->read(argv[1]);

	int W = img1->getWidth();
	int H = img1->getHeight();

	printf("resolution: %d x %d\n", W, H);

	// prepare img2

	myImageData *img2  = new myImageData();
	img2->init(W, H, 1);

	// processing

	int filter_type = std::stoi(argv[2]);
	if (filter_type == 1)
	{
		// Average
		operation(img1, img2, average);
		std::cout << "Average" << '\n';
		img2->save("filter_average");
	} else if (filter_type == 2) {
		// Horizontal edge
		operation(img1, img2, horizontal_edge);
		std::cout << "Horizontal Edge Detection" << '\n';
		img2->save("filter_horizontal_edge");
	} else if (filter_type == 3) {
		// Vertical edge
		operation(img1, img2, verical_edge);
		std::cout << "Vertical Edge Detection" << '\n';
		img2->save("filter_vertical_edge");
	} else if (filter_type == 4) {
		// Laplacian
	  operation(img1, img2, laplacian);
		std::cout << "Laplacian" << '\n';
		img2->save("laplacian");
	} else {
		std::cout << "The input filter type is invalid!!" << '\n';
	}

	delete img1;
	delete img2;

	return 0; // normal termination

}
